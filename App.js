import React, { useState } from 'react';
import {StatusBar, StyleSheet, Text, View, TextInput, Button, Modal} from 'react-native';

export default function App() {
    const [addGoal, setAddGoal] = useState('');
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [selectedGoalId, setSelectedGoalId] = useState(null);
    const [sampleGoals, setSampleGoals] = useState([
        {id: 1, title: 'Faire les courses'},
        {id: 2, title: 'aaaaaaaaaaaaaaa'},
        {id: 3, title: 'bbbbbbbbbbbbb'},
        {id: 4, title: 'cccccccccc'},
        {id: 5, title: 'dddddddddddddd'},
        {id: 6, title: 'eeeeeeeeeeeeee'},
        {id: 7, title: 'fffffffffffff'},
    ]);

    function handleAddGoal() {
        const newGoal = {id: sampleGoals.length + 1, title: addGoal};
        setSampleGoals(currentGoals => [...currentGoals, newGoal]);
        setAddGoal('');
        setIsModalVisible(false);
    }

    function handleDeleteGoal(id) {
        setSampleGoals(currentGoals => currentGoals.filter(goal => goal.id !== id));
        setIsModalVisible(false);
    }

    function openDeleteModal(id) {
        setSelectedGoalId(id);
        setIsModalVisible(true);
    }

    return (
        <View style={styles.container}>
            <Text style={styles.text}>Open up <Text style={styles.textBold}>App.js</Text> to start working on your app!</Text>
            <StatusBar style="auto"/>

            <View style={styles.stepDeux}>
                <TextInput
                    placeholder="placeholder"
                    value={addGoal}
                    onChangeText={text => setAddGoal(text)}
                />
                <Button title="Add" onPress={handleAddGoal}/>

            </View>

            <Modal
                animationType="slide"
                transparent={true}
                visible={isModalVisible}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text style={styles.modalText}>Are you sure you want to delete this goal?</Text>
                        <Button title="Yes" onPress={() => handleDeleteGoal(selectedGoalId)}/>
                        <Button title="No" onPress={() => setIsModalVisible(false)}/>
                    </View>
                </View>
            </Modal>

            <View>
                {sampleGoals.map(goal => (
                    <View key={goal.id}>
                        <Text>{goal.title}</Text>
                        <Button title="Supprimer" onPress={() => openDeleteModal(goal.id)}/>
                    </View>
                ))}
            </View>
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    stepDeux: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    text: {
        color: 'red',
    },
    textBold: {
        fontWeight: 'bold',
    },
});
